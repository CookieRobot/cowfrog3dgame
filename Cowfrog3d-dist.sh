#!/usr/bin/env bash

# compile then archive
bash ./Cowfrog3d-compile.sh
[[ -f "dist/Cowfrog3d/Cowfrog3d.ipk3" ]] && rm "dist/Cowfrog3d/Cowfrog3d.ipk3"
7z a -tzip dist/Cowfrog3d/Cowfrog3d.ipk3 ./Cowfrog3d/* -x!acs/*.acs
