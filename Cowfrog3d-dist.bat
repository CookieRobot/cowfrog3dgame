@echo off

rem compile then archive
call Cowfrog3d-compile.bat
if exist dist\Cowfrog3d\Cowfrog3d.ipk3 del dist\Cowfrog3d\Cowfrog3d.ipk3
tools\win\7za.exe a -tzip dist\Cowfrog3d\Cowfrog3d.ipk3 .\Cowfrog3d\* -x!acs\*.acs
