class CF_StatusBar : BaseStatusBar {
  Font fntBigFont;
  HUDFont mBigFont;
  
  override void Init() {
    Super.Init();
    fullscreenOffsets = true;
    SetSize(0, 640, 480);
    
    fntBigFont = "BigFont";
    mBigFont = HUDFont.create(fntBigFont);
  } // Init
  
  override void Draw(int state, double TicFrac) {
    Super.Draw(state, TicFrac);
    
    BeginHUD(1, true);
    DrawStatusBar(TicFrac);
  } // Draw
  
  void DrawStatusBar(double TicFrac) {
    // portrait
    switch (GetPlayerClassName()) {
      case 'CF_BasePlayer': // Frog
        DrawImage("SBPFROG", (97, -5), DI_ITEM_RIGHT_BOTTOM);
        break;
      case 'CF_CookieTestPlayer': // Flora
        DrawImage("SBPFLORA", (97, -5), DI_ITEM_RIGHT_BOTTOM);
        break;
    }

    // health bar
    DrawBar("SBHPFG", "SBHPBG", CPlayer.Health, 100, (130, -20), 0, SHADER_HORZ, DI_ITEM_LEFT_BOTTOM);
    if (CPlayer.Health > 100) {
      DrawBar("SBHP2FG", "SBHPFG", CPlayer.Health - 100, 100, (130, -20), 0, SHADER_HORZ, DI_ITEM_LEFT_BOTTOM);
    }
    DrawImage("SBCONTHP", (102, -12), DI_ITEM_LEFT_BOTTOM);

    // ammo
    if (CPlayer.ReadyWeapon) {
      switch (CPlayer.ReadyWeapon.GetClassName()) {
        case 'CF_WeaponRifle':
          DrawImage("SBWRFL", (-12, -20), DI_ITEM_RIGHT_BOTTOM);
          // DrawString(mBigFont, FormatNumber(GetAmmoCount("CF_AmmoRifle"), 3), (-12, -24), DI_TEXT_ALIGN_RIGHT);
          break;
      }
    }

    // K/D counters
    DrawImage("SBKILL", (-70, 8), DI_ITEM_LEFT_TOP);
    DrawString(mBigFont, FormatNumber(GetKillCount(), 3), (-13, 18), DI_TEXT_ALIGN_RIGHT);
    DrawImage("SBDEATH", (-70, 39), DI_ITEM_LEFT_TOP);
    DrawString(mBigFont, FormatNumber(GetDeathCount(), 3), (-13, 51), DI_TEXT_ALIGN_RIGHT);
  } // DrawStatusBar

  Name GetPlayerClassName() {
    return CPlayer.mo.GetClassName();
  }

  int GetAmmoCount(string ammoType) {
    let _count = CPlayer.mo.FindInventory(ammoType);
    return _count ? _count.Amount : 0;
  } // int GetAmmoCount

  int GetKillCount() {
    let _count = CPlayer.mo.FindInventory("CF_KillCounter");
    return _count ? _count.Amount : 0;
  } // GetKillCount

  int GetDeathCount() {
    let _count = CPlayer.mo.FindInventory("CF_DeathCounter");
    return _count ? _count.Amount : 0;
  } // GetDeathCount
}