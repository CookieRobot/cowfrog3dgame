class CF_PropAlcohol1 : SwitchingDecoration {
  Default {
    //$Category CowFrog3d/Props
    //$Title Prop Alcohol 1
    //$Sprite ALC1A0
    Health 1;
    Radius 12;
    Height 20;
    Scale 2;
    Activation THINGSPEC_Activate;
    +SHOOTABLE
    +NOBLOOD
    +USESPECIAL
  }
  States {
    Spawn:
      ALC1 A -1;
      Stop;
    Death:
      TNT1 A 1 {
        bUSESPECIAL = false;
        A_NoBlocking();
      }
      ALC1 B -1;
      Stop;
    Active:
      TNT1 A 1 A_DropItem("CF_HealthMoonshine");
      ALC1 A -1;
      Stop;
  }
}

class CF_PropAlcohol2 : CF_PropAlcohol1 {
  Default {
    //$Category CowFrog3d/Props
    //$Title Prop Alcohol 2
    //$Sprite ALC2A0
  }
  States {
    Spawn:
      ALC2 A -1;
      Stop;
    Death:
      TNT1 A 1 {
        bUSESPECIAL = false;
        A_NoBlocking();
      }
      ALC2 B -1;
      Stop;
    Active:
      TNT1 A 1 A_DropItem("CF_HealthMoonshine");
      ALC2 A -1;
      Stop;
  }
}

class CF_PropAlcohol3 : CF_PropAlcohol1 {
  Default {
    //$Category CowFrog3d/Props
    //$Title Prop Alcohol 3
    //$Sprite ALC3A0
  }
  States {
    Spawn:
      ALC3 A -1;
      Stop;
    Death:
      TNT1 A 1 {
        bUSESPECIAL = false;
        A_NoBlocking();
      }
      ALC3 B -1;
      Stop;
    Active:
      TNT1 A 1 A_DropItem("CF_HealthMoonshine");
      ALC3 A -1;
      Stop;
  }
}
