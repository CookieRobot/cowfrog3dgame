class CF_PropTable : CF_BaseProp {
  Default {
    //$Category CowFrog3d/Props
    //$Title Prop Table 1
    //$Sprite TBLEA0
    Radius 13;
    Height 20;
    +SOLID
  }
  States {
    Spawn:
      TBLE A -1;
      Stop;
  }
}
