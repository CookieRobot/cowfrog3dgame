class CF_PropChair : CF_BaseProp {
  Default {
    //$Category CowFrog3d/Props
    //$Title Prop Chair 1
    //$Sprite SIT1A1A5
    Radius 6;
    Height 36;
    +SOLID
  }
  States {
    Spawn:
      SIT1 A -1;
      Stop;
  }
}
