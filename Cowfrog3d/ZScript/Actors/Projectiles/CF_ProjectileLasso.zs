class CF_ProjectileLasso : Actor {
	default {
		Speed 8;
		Radius 10;
		Height 8;
		Damage 2;
		//DamageType "Ice";
		//RenderStyle "Add";
		+NOBLOCKMAP
		+NOGRAVITY
		+DROPOFF
		+MISSILE
		+NOTELEPORT
	}
	
	states {
		Spawn:
			LTST A -1 Bright;
			loop;
		Death:
			LTST A 4 Bright;
			LTST A 2 Bright;
			LTST A 0;
			stop;
	}
	
}