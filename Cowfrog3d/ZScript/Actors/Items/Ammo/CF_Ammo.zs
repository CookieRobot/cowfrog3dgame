// used for fake reload
class CF_BaseAmmo : Inventory {
  Default {
    +INVENTORY.UNTOSSABLE
    +INVENTORY.UNCLEARABLE
    +INVENTORY.INVBAR
    +INVENTORY.KEEPDEPLETED
  }
}

class CF_AmmoSShotgun : CF_BaseAmmo {
  Default {
    Inventory.Amount 1;
    Inventory.MaxAmount 2;
  }
}

class CF_AmmoRevolver : CF_BaseAmmo {
  Default {
    Inventory.Amount 1;
    Inventory.MaxAmount 6;
  }
}
