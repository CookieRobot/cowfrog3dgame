class CF_PickupHat : CustomInventory {
  Default {
    //$Category CowFrog3d/Items/
    //$Title Hat
    //$Sprite HATPA0
    Inventory.PickupMessage "It's Highnoon";
    //Inventory.PickupSound "misc/p_pkup";
    +COUNTITEM
    +INVENTORY.ALWAYSPICKUP
    +INVENTORY.ISHEALTH
  }
  States {
    Spawn:
      HATP A -1;
      Stop;
    Pickup:
      TNT1 A 0 A_GiveInventory("CF_PowerupHighNoon");
      Stop;
  }
}
