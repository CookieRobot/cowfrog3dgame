class CF_HealthCorn : CF_BaseHealth {
  Default {
    //$Category CowFrog3d/Items/Health
    //$Title Corn
    //$Sprite CRNPA0
    Inventory.Amount 10;
    Inventory.MaxAmount 200;
    +INVENTORY.ALWAYSPICKUP;
  }
  States {
    Spawn:
      CRNP A -1;
      Stop;
  }
}
