// these items are simply used as counters
class CF_Counter : Inventory {
  Default {
    Inventory.MaxAmount 0x7FFFFFFF;
    +INVENTORY.UNTOSSABLE
    +INVENTORY.UNCLEARABLE
    -INVENTORY.INVBAR
  }
}

class CF_KillCounter : CF_Counter {
  Default {
    +INVENTORY.UNDROPPABLE
  }
}

class CF_DeathCounter : CF_Counter {
  Default {
    +INVENTORY.UNDROPPABLE
  }
}
