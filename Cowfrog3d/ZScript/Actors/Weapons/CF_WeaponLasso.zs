class CF_WeaponLasso : CF_BaseWeapon {
  Default {
    //$Category CowFrog3d/Weapons
    //$Title Lasso
    //$Sprite LTSTA0
    Weapon.SelectionOrder 400;
    Weapon.SlotNumber 5;
    Weapon.AmmoUse 0;
    Weapon.AmmoGive 64;
    Weapon.AmmoType "Cell";
  }
  States {
    Spawn: // This state is entered when you drop the weapon.
      LTST A -1;
      Stop;
    Ready: // This state is entered when you have this weapon selected.
      LTST A 1 A_WeaponReady;
      Loop;
    Deselect: // This state is entered when you deselect the current selected weapon.
      LTST A 1 A_Lower;
      Loop;
    Select: // This state is entered when you select this weapon.
      LTST A 1 A_Raise;
      Loop;
    Fire: // The firing state.
      LTST A 3;
      LTST A 1 {
        Actor testDumm;
        if (FindInventory("CF_PowerupDrunk")) {
          A_Log("Threw Lasso drunk");
          testDumm = A_FireProjectile("CF_ProjectileLasso",Random (-30, 30),1,8,8,0);
        } else {
          A_Log("Threw Lasso Sober");
          testDumm = A_FireProjectile("CF_ProjectileLasso");
        }
        invoker.setLassoActor(testDumm);
      }
      goto Thrown;
    Thrown:
      TNT1 A 30 {
        if (invoker.holdingLasso()) {
          Player.SetPSprite(PSP_WEAPON,invoker.FindState("Select"));
        }
      }
      Loop;
    Flash: // The weapons flash.
      LTST A 6 ;
      Stop;
  }

  Actor lassoObject;
  void setLassoActor(Actor send) {
    lassoObject = send;
  }

  bool holdingLasso() {
    return lassoObject == null;
  }
}
