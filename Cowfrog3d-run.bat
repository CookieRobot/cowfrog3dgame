call Cowfrog3d-dist.bat

if not exist port\gzdoom\gzdoom.exe (
  echo Sourceport "port\gzdoom" not found!
  goto end
)

port\gzdoom\gzdoom.exe -iwad dist\Cowfrog3d\Cowfrog3d.ipk3

:end
